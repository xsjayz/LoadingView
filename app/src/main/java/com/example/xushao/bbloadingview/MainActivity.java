package com.example.xushao.bbloadingview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private LoadingDialog mLoadingDialog;
    private String mLastMsg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LoadingView loadingView = (LoadingView) findViewById(R.id.loading_view);

        Button show = (Button) findViewById(R.id.show_loading);
        Button hide = (Button) findViewById(R.id.hide_loading);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingView.setVisibility(View.VISIBLE);
                dismissLoadingDialog();
            }
        });

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingView.setVisibility(View.GONE);
                showLoadingDialog();
            }
        });
    }

    public void showLoadingDialog() {
        showLoadingDialog("加载中...");
    }

    public void showLoadingDialog(int strId) {
        showLoadingDialog(getString(strId), true);
    }

    public void showLoadingDialog(String msg) {
        showLoadingDialog(msg, true);
    }

    public void showLoadingDialog(int strId, boolean flag) {
        showLoadingDialog(getString(strId), flag);
    }

    public void showLoadingDialog(String msg, boolean flag) {
        if ((mLoadingDialog != null && mLoadingDialog.isShowing()) && TextUtils.equals(msg, mLastMsg)) {
            return;
        }
        dismissLoadingDialog();
        mLastMsg = msg;
        mLoadingDialog = new LoadingDialog(this, R.style.LoadingDialogTheme, msg);
        mLoadingDialog.setCancelable(flag);
        mLoadingDialog.show();
    }

    public void dismissLoadingDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

}
