package com.example.xushao.bbloadingview.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * 工具类
 *
 * @author shaojie.xu
 */
public class Utils {

    public static float dp2px(Context context, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

}
